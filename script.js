//////////////////////////
// PROJECT IMAGES MODAL //
//////////////////////////
const MODAL = document.getElementById('image-modal');
const MODAL_CLOSE = document.getElementById('image-modal__close');
const MODAL_IMG = document.getElementById('image-modal__img');
const MODAL_CAPTION = document.getElementById('image-modal__caption');
const PROJECT_IMGS = document.querySelectorAll('.project__img');

const enlargeImg = (e) => {
  MODAL.style.display = 'block';
  document.body.style.overflow = 'hidden';

  if (e.target.id === 'movieSearch_img') {
    MODAL_IMG.src = './images/mobile_screen.png';
  } else if (e.target.id === 'Portfolio_img') {
    MODAL_IMG.src = '';
  } else {
    MODAL_IMG.src = e.target.src;
  }

  MODAL_IMG.alt = e.target.alt;

  if (e.target.id === 'Portfolio_img') {
    MODAL_CAPTION.innerHTML = 'Nothing to see here';
  } else {
    MODAL_CAPTION.innerHTML = e.target.alt;
  }
};

PROJECT_IMGS.forEach((img) => img.addEventListener('click', enlargeImg));

// Close the modal
MODAL.onclick = () => {
  document.body.style.overflow = 'unset';
  MODAL.style.display = 'none';
};

MODAL_CLOSE.onclick = function () {
  document.body.style.overflow = 'unset';
  MODAL.style.display = 'none';
};

///////////////////
// SMOOTH SCROLL //
///////////////////
const NAV = document.querySelector('nav');
const LINKS = document.querySelectorAll('.nav-menu__a');
const navHeight = NAV.scrollHeight;

function scroll(e) {
  e.preventDefault();

  const targetId = e.target.getAttribute('href');
  const destination = document.querySelector(targetId).offsetTop;

  window.scrollTo({
    top: destination - navHeight / 4,
    left: 0,
    behavior: 'smooth',
  });
}

LINKS.forEach((link) => link.addEventListener('click', scroll));
